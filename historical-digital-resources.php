<?php
/*
Plugin Name: Historical Digital Resources
Plugin URI: http://wpalchemists.com
Description: Create a searchable database of websites about historical resources
Version: 1
Author: Morgan Kay
Author URI: http://wpalchemists.com
Text Domain: historical-digital-resources
*/

/*  Copyright 2015 Morgan Kay and the Medieval Academy of America (email : morgan@wpalchemists.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// ------------------------------------------------------------------------
// REQUIRE MINIMUM VERSION OF WORDPRESS:                                               
// ------------------------------------------------------------------------


function hdr_requires_wordpress_version() {
    global $wp_version;
    $plugin = plugin_basename( __FILE__ );
    $plugin_data = get_plugin_data( __FILE__, false );

    if ( version_compare($wp_version, "4.0", "<" ) ) {
        if( is_plugin_active($plugin) ) {
            deactivate_plugins( $plugin );
            wp_die( "'".$plugin_data['Name']."'requires WordPress 4.0 or higher, and has been deactivated! Please upgrade WordPress and try again.<br /><br />Back to <a href='".admin_url()."'>WordPress admin</a>." );
        }
    }
}
add_action( 'admin_init', 'hdr_requires_wordpress_version');

// ------------------------------------------------------------------------
// REGISTER HOOKS & CALLBACK FUNCTIONS:
// ------------------------------------------------------------------------

// Set-up Action and Filter Hooks
// register_activation_hook( __FILE__, 'hdr_add_defaults');
// register_uninstall_hook( __FILE__, 'hdr_delete_plugin_options');
// add_action( 'admin_init', 'hdr_init');
// add_action( 'admin_menu', 'hdr_add_options_page');
// add_filter( 'plugin_action_links_'. plugin_basename(__FILE__), 'hdr_plugin_action_links', 10, 2 );

// Require options stuff
// require_once( plugin_dir_path( __FILE__ ) . 'options.php');
// Require views
require_once( plugin_dir_path( __FILE__ ) . 'views.php');
// Require language insertion
require_once( plugin_dir_path( __FILE__ ) . 'includes/languages.php');

// Initialize language so it can be translated
function hdr_language_init() {
  load_plugin_textdomain( 'historical-digital-resources', false, dirname(plugin_basename(__FILE__)) . 'languages');
}
add_action('init', 'hdr_language_init');


// ------------------------------------------------------------------------
// REGISTER TAXONOMIES AND POST TYPES
// ------------------------------------------------------------------------

// Register Custom Taxonomy: Subject
function hdr_register_taxonomy_subject() {

    $labels = array(
        'name'                       => _x( 'Subjects', 'Taxonomy General Name', 'historical-digital-resources' ),
        'singular_name'              => _x( 'Subject', 'Taxonomy Singular Name', 'historical-digital-resources' ),
        'menu_name'                  => __( 'Subjects', 'historical-digital-resources' ),
        'all_items'                  => __( 'All Subjects', 'historical-digital-resources' ),
        'parent_item'                => __( 'Parent Subject', 'historical-digital-resources' ),
        'parent_item_colon'          => __( 'Parent Subject:', 'historical-digital-resources' ),
        'new_item_name'              => __( 'New Subject', 'historical-digital-resources' ),
        'add_new_item'               => __( 'Add New Subject', 'historical-digital-resources' ),
        'edit_item'                  => __( 'Edit Subject', 'historical-digital-resources' ),
        'update_item'                => __( 'Update Subject', 'historical-digital-resources' ),
        'view_item'                  => __( 'View Subject', 'historical-digital-resources' ),
        'separate_items_with_commas' => __( 'Separate Subjects with commas', 'historical-digital-resources' ),
        'add_or_remove_items'        => __( 'Add or remove Subjects', 'historical-digital-resources' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'historical-digital-resources' ),
        'popular_items'              => __( 'Popular Subjects', 'historical-digital-resources' ),
        'search_items'               => __( 'Search Subjects', 'historical-digital-resources' ),
        'not_found'                  => __( 'Not Found', 'historical-digital-resources' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => false,
        'show_in_nav_menus'          => false,
        'show_tagcloud'              => false,
    );
    register_taxonomy( 'subject', array( 'resource' ), $args );

}
add_action( 'init', 'hdr_register_taxonomy_subject', 0 );

// Register Custom Taxonomy: Genre
function hdr_register_taxonomy_genre() {

    $labels = array(
        'name'                       => _x( 'Types of Medieval Source Evidence', 'Taxonomy General Name', 'historical-digital-resources' ),
        'singular_name'              => _x( 'Type of Medieval Source Evidence', 'Taxonomy Singular Name', 'historical-digital-resources' ),
        'menu_name'                  => __( 'Types of Source Evidence', 'historical-digital-resources' ),
        'all_items'                  => __( 'All Types of Source Evidence', 'historical-digital-resources' ),
        'parent_item'                => __( 'Parent Type of Source Evidence', 'historical-digital-resources' ),
        'parent_item_colon'          => __( 'Parent Type of Source Evidence:', 'historical-digital-resources' ),
        'new_item_name'              => __( 'New Type of Source Evidence', 'historical-digital-resources' ),
        'add_new_item'               => __( 'Add New Type of Source Evidence', 'historical-digital-resources' ),
        'edit_item'                  => __( 'Edit Type of Source Evidence', 'historical-digital-resources' ),
        'update_item'                => __( 'Update Type of Source Evidence', 'historical-digital-resources' ),
        'view_item'                  => __( 'View Type of Source Evidence', 'historical-digital-resources' ),
        'separate_items_with_commas' => __( 'Separate Types with commas', 'historical-digital-resources' ),
        'add_or_remove_items'        => __( 'Add or remove Types', 'historical-digital-resources' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'historical-digital-resources' ),
        'popular_items'              => __( 'Popular Types', 'historical-digital-resources' ),
        'search_items'               => __( 'Search Types', 'historical-digital-resources' ),
        'not_found'                  => __( 'Not Found', 'historical-digital-resources' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => false,
        'show_in_nav_menus'          => false,
        'show_tagcloud'              => false,
    );
    register_taxonomy( 'genre', array( 'resource' ), $args );

}
add_action( 'init', 'hdr_register_taxonomy_genre', 0 );

// Register Custom Taxonomy: Region
function hdr_register_taxonomy_region() {

    $labels = array(
        'name'                       => _x( 'Geo-political Regions', 'Taxonomy General Name', 'historical-digital-resources' ),
        'singular_name'              => _x( 'Geo-political Region', 'Taxonomy Singular Name', 'historical-digital-resources' ),
        'menu_name'                  => __( 'Geo-political Regions', 'historical-digital-resources' ),
        'all_items'                  => __( 'All Regions', 'historical-digital-resources' ),
        'parent_item'                => __( 'Parent Region', 'historical-digital-resources' ),
        'parent_item_colon'          => __( 'Parent Region:', 'historical-digital-resources' ),
        'new_item_name'              => __( 'New Region Name', 'historical-digital-resources' ),
        'add_new_item'               => __( 'Add New Region', 'historical-digital-resources' ),
        'edit_item'                  => __( 'Edit Region', 'historical-digital-resources' ),
        'update_item'                => __( 'Update Region', 'historical-digital-resources' ),
        'view_item'                  => __( 'View Region', 'historical-digital-resources' ),
        'separate_items_with_commas' => __( 'Separate Regions with commas', 'historical-digital-resources' ),
        'add_or_remove_items'        => __( 'Add or remove Regions', 'historical-digital-resources' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'historical-digital-resources' ),
        'popular_items'              => __( 'Popular Regions', 'historical-digital-resources' ),
        'search_items'               => __( 'Search Regions', 'historical-digital-resources' ),
        'not_found'                  => __( 'Not Found', 'historical-digital-resources' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => false,
        'show_in_nav_menus'          => false,
        'show_tagcloud'              => false,
    );
    register_taxonomy( 'region', array( 'resource' ), $args );

}
add_action( 'init', 'hdr_register_taxonomy_region', 0 );

// Register Custom Taxonomy: Modern Language
function hdr_register_taxonomy_modern_language() {

    $labels = array(
        'name'                       => _x( 'Modern Languages', 'Taxonomy General Name', 'historical-digital-resources' ),
        'singular_name'              => _x( 'Modern Language', 'Taxonomy Singular Name', 'historical-digital-resources' ),
        'menu_name'                  => __( 'Modern Languages', 'historical-digital-resources' ),
        'all_items'                  => __( 'All Modern Languages', 'historical-digital-resources' ),
        'parent_item'                => __( 'Parent Modern Language', 'historical-digital-resources' ),
        'parent_item_colon'          => __( 'Parent Modern Language:', 'historical-digital-resources' ),
        'new_item_name'              => __( 'New Modern Language Name', 'historical-digital-resources' ),
        'add_new_item'               => __( 'Add New Modern Language', 'historical-digital-resources' ),
        'edit_item'                  => __( 'Edit Modern Language', 'historical-digital-resources' ),
        'update_item'                => __( 'Update Modern Language', 'historical-digital-resources' ),
        'view_item'                  => __( 'View Modern Language', 'historical-digital-resources' ),
        'separate_items_with_commas' => __( 'Separate Modern Languages with commas', 'historical-digital-resources' ),
        'add_or_remove_items'        => __( 'Add or remove Modern Languages', 'historical-digital-resources' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'historical-digital-resources' ),
        'popular_items'              => __( 'Popular Modern Languages', 'historical-digital-resources' ),
        'search_items'               => __( 'Search Modern Languages', 'historical-digital-resources' ),
        'not_found'                  => __( 'Not Found', 'historical-digital-resources' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => false,
        'show_in_nav_menus'          => false,
        'show_tagcloud'              => false,
    );
    register_taxonomy( 'modern_language', array( 'resource' ), $args );

    hdr_insert_language_taxonomy_terms( 'modern_language' ); 

}
add_action( 'init', 'hdr_register_taxonomy_modern_language', 0 );

// Register Custom Taxonomy: Translation Language
function hdr_register_taxonomy_translation_language() {

    $labels = array(
        'name'                       => _x( 'Modern Translation Languages', 'Taxonomy General Name', 'historical-digital-resources' ),
        'singular_name'              => _x( 'Modern Translation Language', 'Taxonomy Singular Name', 'historical-digital-resources' ),
        'menu_name'                  => __( 'Modern Translation Languages', 'historical-digital-resources' ),
        'all_items'                  => __( 'All Translation Languages', 'historical-digital-resources' ),
        'parent_item'                => __( 'Parent Translation Language', 'historical-digital-resources' ),
        'parent_item_colon'          => __( 'Parent Translation Language:', 'historical-digital-resources' ),
        'new_item_name'              => __( 'New Translation Language', 'historical-digital-resources' ),
        'add_new_item'               => __( 'Add New Translation Language', 'historical-digital-resources' ),
        'edit_item'                  => __( 'Edit Translation Language', 'historical-digital-resources' ),
        'update_item'                => __( 'Update Translation Language', 'historical-digital-resources' ),
        'view_item'                  => __( 'View Translation Language', 'historical-digital-resources' ),
        'separate_items_with_commas' => __( 'Separate Translation Languages with commas', 'historical-digital-resources' ),
        'add_or_remove_items'        => __( 'Add or remove Translation Languages', 'historical-digital-resources' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'historical-digital-resources' ),
        'popular_items'              => __( 'Popular Translation Languages', 'historical-digital-resources' ),
        'search_items'               => __( 'Search Translation Languages', 'historical-digital-resources' ),
        'not_found'                  => __( 'Not Found', 'historical-digital-resources' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => false,
        'show_in_nav_menus'          => false,
        'show_tagcloud'              => false,
    );
    register_taxonomy( 'translation_language', array( 'resource' ), $args );

    hdr_insert_language_taxonomy_terms( 'translation_language' );

}
add_action( 'init', 'hdr_register_taxonomy_translation_language', 0 );

// Register Custom Taxonomy: Original Language
function hdr_register_taxonomy_original_language() {

    $labels = array(
        'name'                       => _x( 'Original Languages', 'Taxonomy General Name', 'historical-digital-resources' ),
        'singular_name'              => _x( 'Original Language', 'Taxonomy Singular Name', 'historical-digital-resources' ),
        'menu_name'                  => __( 'Original Languages', 'historical-digital-resources' ),
        'all_items'                  => __( 'All Original Languages', 'historical-digital-resources' ),
        'parent_item'                => __( 'Parent Original Language', 'historical-digital-resources' ),
        'parent_item_colon'          => __( 'Parent Original Language:', 'historical-digital-resources' ),
        'new_item_name'              => __( 'New Original Language Name', 'historical-digital-resources' ),
        'add_new_item'               => __( 'Add New Original Language', 'historical-digital-resources' ),
        'edit_item'                  => __( 'Edit Original Language', 'historical-digital-resources' ),
        'update_item'                => __( 'Update Original Language', 'historical-digital-resources' ),
        'view_item'                  => __( 'View Original Language', 'historical-digital-resources' ),
        'separate_items_with_commas' => __( 'Separate Original Languages with commas', 'historical-digital-resources' ),
        'add_or_remove_items'        => __( 'Add or remove Original Languages', 'historical-digital-resources' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'historical-digital-resources' ),
        'popular_items'              => __( 'Popular Original Languages', 'historical-digital-resources' ),
        'search_items'               => __( 'Search Original Languages', 'historical-digital-resources' ),
        'not_found'                  => __( 'Not Found', 'historical-digital-resources' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => false,
        'show_in_nav_menus'          => false,
        'show_tagcloud'              => false,
    );
    register_taxonomy( 'original_language', array( 'resource' ), $args );

}
add_action( 'init', 'hdr_register_taxonomy_original_language', 0 );

// Register Custom Taxonomy: License
function hdr_register_taxonomy_license() {

    $labels = array(
        'name'                      => _x( 'Licenses', 'Taxonomy General Name', 'historical-digital-resources'),
        'singular_name'             => _x( 'License', 'Taxonomy Singular Name', 'historical-digital-resources'),
        'menu_name'                 => __( 'Licenses', 'historical-digital-resources'),
        'all_items'                 => __( 'All Licenses', 'historical-digital-resources'),
        'parent_item'               => __( 'Parent License', 'historical-digital-resources'),
        'parent_item_colon'         => __( 'Parent License:', 'historical-digital-resources'),
        'new_item_name'             => __( 'New License Name', 'historical-digital-resources'),
        'add_new_item'              => __( 'Add New License', 'historical-digital-resources'),
        'edit_item'                 => __( 'Edit License', 'historical-digital-resources'),
        'update_item'               => __( 'Update License', 'historical-digital-resources'),
        'view_item'                 => __( 'View License', 'historical-digital-resources'),
        'separate_items_with_commas'=> __( 'Separate licenses with commas', 'historical-digital-resources'),
        'add_or_remove_items'       => __( 'Add or remove licenses ', 'historical-digital-resources'),
        'choose_from_most_used'     => __( 'Choose from the most used licenses', 'historical-digital-resources'),
        'popular_items'             => __( 'Popular Licenses', 'historical-digital-resources'),
        'search_items'              => __( 'Search Licenses', 'historical-digital-resources'),
        'not_found'                 => __( 'Not Found', 'historical-digital-resources'),
    );
    $args = array(
        'labels'                    => $labels,
        'hierarchical'              => true,
        'public'                    => true,
        'show_ui'                   => true,
        'show_admin_column'         => false,
        'show_in_nav_menus'         => false,
        'show_tagcloud'             => false,
    );
    register_taxonomy( 'license', array( 'resource'), $args );

}
add_action( 'init', 'hdr_register_taxonomy_license', 0 );

// Register Custom Taxonomy: Resource Type
function hdr_register_taxonomy_resource_type() {

    $labels = array(
        'name'                       => _x( 'Digital Resource Types', 'Taxonomy General Name', 'historical-digital-resources' ),
        'singular_name'              => _x( 'Digital Resource Type', 'Taxonomy Singular Name', 'historical-digital-resources' ),
        'menu_name'                  => __( 'Digital Resource Types', 'historical-digital-resources' ),
        'all_items'                  => __( 'All Digital Resource Types', 'historical-digital-resources' ),
        'parent_item'                => __( 'Parent Digital Resource Type', 'historical-digital-resources' ),
        'parent_item_colon'          => __( 'Parent Digital Resource Type:', 'historical-digital-resources' ),
        'new_item_name'              => __( 'New Digital Resource Type Name', 'historical-digital-resources' ),
        'add_new_item'               => __( 'Add New Digital Resource Type', 'historical-digital-resources' ),
        'edit_item'                  => __( 'Edit Digital Resource Type', 'historical-digital-resources' ),
        'update_item'                => __( 'Update Digital Resource Type', 'historical-digital-resources' ),
        'view_item'                  => __( 'View Digital Resource Type', 'historical-digital-resources' ),
        'separate_items_with_commas' => __( 'Separate Digital Resource Types with commas', 'historical-digital-resources' ),
        'add_or_remove_items'        => __( 'Add or remove Digital Resource Types', 'historical-digital-resources' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'historical-digital-resources' ),
        'popular_items'              => __( 'Popular Digital Resource Types', 'historical-digital-resources' ),
        'search_items'               => __( 'Search Digital Resource Types', 'historical-digital-resources' ),
        'not_found'                  => __( 'Not Found', 'historical-digital-resources' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => false,
        'show_in_nav_menus'          => false,
        'show_tagcloud'              => false,
    );
    register_taxonomy( 'resource_type', array( 'resource' ), $args );

}
add_action( 'init', 'hdr_register_taxonomy_resource_type', 0 );

// Register Custom Alphabet
function hdr_register_taxonomy_alphabet() {

    $labels = array(
        'name'                       => _x( 'Alphabets', 'Taxonomy General Name', 'historical-digital-resources' ),
        'singular_name'              => _x( 'Alphabet', 'Taxonomy Singular Name', 'historical-digital-resources' ),
        'menu_name'                  => __( 'Alphabet', 'historical-digital-resources' ),
        'all_items'                  => __( 'All Alphabets', 'historical-digital-resources' ),
        'parent_item'                => __( 'Parent Alphabet', 'historical-digital-resources' ),
        'parent_item_colon'          => __( 'Parent Alphabet:', 'historical-digital-resources' ),
        'new_item_name'              => __( 'New Item Alphabet', 'historical-digital-resources' ),
        'add_new_item'               => __( 'Add New Alphabet', 'historical-digital-resources' ),
        'edit_item'                  => __( 'Edit Alphabet', 'historical-digital-resources' ),
        'update_item'                => __( 'Update Alphabet', 'historical-digital-resources' ),
        'view_item'                  => __( 'View Alphabet', 'historical-digital-resources' ),
        'separate_items_with_commas' => __( 'Separate Alphabets with commas', 'historical-digital-resources' ),
        'add_or_remove_items'        => __( 'Add or remove Alphabets', 'historical-digital-resources' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'historical-digital-resources' ),
        'popular_items'              => __( 'Popular Alphabets', 'historical-digital-resources' ),
        'search_items'               => __( 'Search Alphabets', 'historical-digital-resources' ),
        'not_found'                  => __( 'Not Found', 'historical-digital-resources' ),
        'items_list'                 => __( 'Alphabets list', 'historical-digital-resources' ),
        'items_list_navigation'      => __( 'Alphabetslist navigation', 'historical-digital-resources' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => true,
        'show_ui'                    => false,
        'show_admin_column'          => false,
        'show_in_nav_menus'          => false,
        'show_tagcloud'              => false,
    );
    register_taxonomy( 'alphabet', array( 'resource' ), $args );

}
add_action( 'init', 'hdr_register_taxonomy_alphabet', 0 );

// Register Custom Post Type: Resource
function hdr_register_cpt_resource() {

    $labels = array(
        'name'               => _x( 'Resources', 'Post Type General Name', 'historical-digital-resources'),
        'singular_name'      => _x( 'Resource', 'Post Type Singular Name', 'historical-digital-resources'),
        'menu_name'          => __( 'Resources', 'historical-digital-resources'),
        'name_admin_bar'     => __( 'Resource', 'historical-digital-resources'),
        'parent_item_colon'  => __( 'Parent Resource:', 'historical-digital-resources'),
        'all_items'          => __( 'All Resources', 'historical-digital-resources'),
        'add_new_item'       => __( 'Add New Resource', 'historical-digital-resources'),
        'add_new'            => __( 'Add New Resource', 'historical-digital-resources'),
        'new_item'           => __( 'New Resource', 'historical-digital-resources'),
        'edit_item'          => __( 'Edit Resource', 'historical-digital-resources'),
        'update_item'        => __( 'Update Resource', 'historical-digital-resources'),
        'view_item'          => __( 'View Resource', 'historical-digital-resources'),
        'search_items'       => __( 'Search Resource', 'historical-digital-resources'),
        'not_found'          => __( 'Not found', 'historical-digital-resources'),
        'not_found_in_trash' => __( 'Not found in Trash', 'historical-digital-resources'),
    );
    $args = array(
        'label'              => __( 'resource', 'historical-digital-resources'),
        'description'        => __( 'Online resource about the Middle Ages', 'historical-digital-resources'),
        'labels'             => $labels,
        'supports'           => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', ),
        'taxonomies'         => array( 'license', 'resource_type', 'original_language', 'modern_language', 'translation_language', 'region', 'genre', 'subject', 'alphabet' ),
        'hierarchical'       => true,
        'public'             => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'menu_position'      => 5,
        'menu_icon'          => 'dashicons-book',
        'show_in_admin_bar'  => true,
        'show_in_nav_menus'  => false,
        'can_export'         => true,
        'has_archive'        => true,      
        'exclude_from_search'=> false,
        'publicly_queryable' => true,
        'capability_type'    => 'page',
    );
    register_post_type( 'resource', $args );

}
add_action( 'init', 'hdr_register_cpt_resource', 0 );

// Register Custom Post Type
function hdr_register_cpt_creator() {

    $labels = array(
        'name'                => _x( 'Medieval Creators', 'Post Type General Name', 'historical-digital-resources' ),
        'singular_name'       => _x( 'Medieval Creator', 'Post Type Singular Name', 'historical-digital-resources' ),
        'menu_name'           => __( 'Medieval Creators', 'historical-digital-resources' ),
        'name_admin_bar'      => __( 'Medieval Creator', 'historical-digital-resources' ),
        'parent_item_colon'   => __( 'Parent Creator:', 'historical-digital-resources' ),
        'all_items'           => __( 'All Creators', 'historical-digital-resources' ),
        'add_new_item'        => __( 'Add New Creator', 'historical-digital-resources' ),
        'add_new'             => __( 'Add New', 'historical-digital-resources' ),
        'new_item'            => __( 'New Creator', 'historical-digital-resources' ),
        'edit_item'           => __( 'Edit Creator', 'historical-digital-resources' ),
        'update_item'         => __( 'Update Creator', 'historical-digital-resources' ),
        'view_item'           => __( 'View Creator', 'historical-digital-resources' ),
        'search_items'        => __( 'Search Creators', 'historical-digital-resources' ),
        'not_found'           => __( 'Not found', 'historical-digital-resources' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'historical-digital-resources' ),
    );
    $args = array(
        'label'               => __( 'creator', 'historical-digital-resources' ),
        'description'         => __( 'Authors, artists, and other historical creators.', 'historical-digital-resources' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions' ),
        // 'taxonomies'          => array( 'region' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-universal-access',
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => true,
        'can_export'          => true,
        'has_archive'         => true,      
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );
    register_post_type( 'creator', $args );

}

// Hook into the 'init' action
add_action( 'init', 'hdr_register_cpt_creator', 0 );

// ------------------------------------------------------------------------
// SET UP META BOXES
// ------------------------------------------------------------------------

if(!class_exists('WPAlchemy_MetaBox')) { //only include metabox files if another plugin hasn't done it
    include_once 'includes/MetaBox.php';
}

define( 'HDR_PATH', plugin_dir_path(__FILE__) );

// Enqueue styles and scripts
function hdr_admin_scripts_and_styles($hook)
{
    if ( is_admin() ) {
        wp_enqueue_style( 'hdr-styles', plugins_url() . '/historical-digital-resources/css/historical-digital-resources-admin.css' );
    }
}
add_action( 'admin_enqueue_scripts', 'hdr_admin_scripts_and_styles' );

// Create metabox for location/address information
$resource_metabox = new WPAlchemy_MetaBox(array
(
    'id' => 'resource_meta',
    'title' => 'Resource Details',
    'types' => array('resource'),
    'template' => HDR_PATH . '/includes/resource-meta.php',
    'mode' => WPALCHEMY_MODE_EXTRACT,
    'prefix' => '_hdr_'
));

$creator_meta = new WPAlchemy_MetaBox(array
(
    'id' => 'creator_meta',
    'title' => 'Creator Details',
    'types' => array('creator'),
    'template' => HDR_PATH . '/includes/creator-meta.php',
    'mode' => WPALCHEMY_MODE_EXTRACT,
    'prefix' => '_hdr_'
));


// ------------------------------------------------------------------------
// CONNECT RESOURCES TO CREATORS
// https://github.com/scribu/wp-posts-to-posts/blob/master/posts-to-posts.php
// ------------------------------------------------------------------------

function hdr_p2p_check() {
    if ( !is_plugin_active( 'posts-to-posts/posts-to-posts.php' ) ) {
        require_once dirname( __FILE__ ) . '/includes/wpp2p/autoload.php';
        define( 'P2P_PLUGIN_VERSION', '1.6.3' );
        define( 'P2P_TEXTDOMAIN', 'historical-digital-resources' );
    }
}
add_action( 'admin_init', 'hdr_p2p_check' );

function hdr_p2p_load() {
    //load_plugin_textdomain( P2P_TEXTDOMAIN, '', basename( dirname( __FILE__ ) ) . '/languages' );
    if ( !function_exists( 'p2p_register_connection_type' ) ) {
        require_once dirname( __FILE__ ) . '/includes/wpp2p/autoload.php';
    }
    P2P_Storage::init();
    P2P_Query_Post::init();
    P2P_Query_User::init();
    P2P_URL_Query::init();
    P2P_Widget::init();
    P2P_Shortcodes::init();
    register_uninstall_hook( __FILE__, array( 'P2P_Storage', 'uninstall' ) );
    if ( is_admin() )
        hdr_load_admin();
}

function hdr_load_admin() {
    P2P_Autoload::register( 'P2P_', dirname( __FILE__ ) . '/includes/wpp2p/admin' );

    new P2P_Box_Factory;
    new P2P_Column_Factory;
    new P2P_Dropdown_Factory;
}

function hdr_p2p_init() {
    // Safe hook for calling p2p_register_connection_type()
    do_action( 'p2p_init' );
}

require dirname( __FILE__ ) . '/includes/wpp2p/scb/load.php';
scb_init( 'hdr_p2p_load' );
add_action( 'wp_loaded', 'hdr_p2p_init' );

function hdr_create_connections() {
    p2p_register_connection_type( array(
        'name' => 'resources_to_creators',
        'from' => 'resource',
        'to' => 'creator',
        'cardinality' => 'many-to-many',
        'to_labels' => array(
            'singular_name' => __( 'Creator', 'wpaesm' ),
            'search_items' => __( 'Search creators', 'wpaesm' ),
            'not_found' => __( 'No creators found.', 'wpaesm' ),
            'create' => __( 'Add Creator', 'wpaesm' ),
        ),
    ) );

}
add_action( 'p2p_init', 'hdr_create_connections' );

// ------------------------------------------------------------------------
// ADD "LIVE" FIELD TO RESOURCES OVERVIEW PAGE
// https://github.com/bamadesigner/manage-wordpress-posts-using-bulk-edit-and-quick-edit
// ------------------------------------------------------------------------

add_filter( 'manage_resource_posts_columns', 'hdr_create_resources_live_column', 10, 2 );
function hdr_create_resources_live_column( $columns ) {

    $columns[ 'hdr_live' ] = 'Live';
        
    return $columns;
    
}

add_filter( 'manage_edit-resource_sortable_columns', 'hdr_make_live_column_sortable' );
function hdr_make_live_column_sortable( $sortable_columns ) {

    $sortable_columns[ 'hdr_live' ] = '_hdr_live';

    return $sortable_columns;
    
}


add_action( 'manage_resource_posts_custom_column', 'hdr_populate_live_column', 10, 2 );
function hdr_populate_live_column( $column_name, $post_id ) {

    switch( $column_name ) {
    
        case 'hdr_live':
            $live = get_post_meta( $post_id, '_hdr_live', true );
            if( '1' == $live ) {
                echo '<div id="live-' . $post_id . '">' . __( 'Live', 'historical-digital-resources' ) . '</div>';
            } else {
                echo '<div id="live-' . $post_id . '">' . __( 'Hidden', 'historical-digital-resources' ) . '</div>';
            }
        
            break;
            
    }
    
}


add_action( 'pre_get_posts', 'hdr_sort_by_live_status', 1 );
function hdr_sort_by_live_status( $query ) {

    if ( $query->is_main_query() && ( $orderby = $query->get( 'orderby' ) ) ) {
    
        switch( $orderby ) {
        
            case 'hdr_live':
                $query->set( 'meta_key', '_hdr_live' );
                $query->set( 'orderby', 'meta_value' );
                
                break;
                
        }
    
    }
    
}


// ------------------------------------------------------------------------
// ALPHABETIZE RESOURCES
// http://wordpress.stackexchange.com/questions/67271/display-all-posts-starting-with-given-letter
// ------------------------------------------------------------------------

add_action( 'save_post', 'hdr_alphabetize_resource' );
function hdr_alphabetize_resource( $post_id ) {

    $post = get_post( $post_id );

    if ( 'resource' != $post->post_type ) {
        return;
    }

    if( isset( $_POST['post_title'] )  ) {
        $title = $_POST['post_title'];
    } else {
        $title = '';
    }

    $articles = array(
        'a',
        'an',
        'the',
    );

    $first_word = strtolower( strtok( $title, " " ) );

    if( in_array( $first_word, $articles ) ) {
        $title = substr( strstr( $title, " " ), 1 );
    }

    $first_letter = substr( $title, 0, 1 );

    wp_set_post_terms( $post_id, $first_letter, 'alphabet', false );
}

?>
