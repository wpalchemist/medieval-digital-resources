<div class="hdr-metabox">

<!-- Alternate Names/Spellings -->
	<label><?php _e( 'Alternate Names/Spellings', 'historical-digital-resources'); ?></label>
	<span class="info"><?php _e( 'Enter a comma-separated list of all alternate spellings or alternate names for this person.', 'historical-digital-resources' ); ?></span>
	<p>
		<input type="text" class="widefat" name="<?php $metabox->the_name( 'alias' ); ?>" value="<?php $metabox->the_value( 'alias' ); ?>"/>
	</p>

<!-- Title -->
	<label><?php _e( 'Title', 'historical-digital-resources'); ?></label>
	<span class="info"><?php _e( 'If the creator has a title, such as King, Bishop, Abbess, or Queen, enter it here.', 'historical-digital-resources' ); ?></span>
	<p>
		<input type="text" class="widefat" name="<?php $metabox->the_name( 'title' ); ?>" value="<?php $metabox->the_value( 'title' ); ?>"/>
	</p>

<!-- Dates -->
	<label><?php _e( 'Dates', 'historical-digital-resources' ); ?></label>
	<span class="info"><?php _e( 'Enter the birth and death dates of the creator (use "c" to indicate approximate dates).', 'historical-digital-resources' ); ?></span>
	<p>
		<?php _e( 'From', 'historical-digital-resources' ); ?><input type="text" class="date" name="<?php $metabox->the_name( 'start_date' ); ?>" value="<?php $metabox->the_value( 'start_date' ); ?>"/>
		&nbsp;<?php _e( 'To', 'historical-digital-resources' ); ?>&nbsp;<input type="text" class="date" name="<?php $metabox->the_name( 'end_date' ); ?>" value="<?php $metabox->the_value( 'end_date' ); ?>"/>
	</p>
	<label><?php _e( 'Date Notes', 'historical-digital-resources'); ?></label>
	<span class="info"><?php _e( 'Enter any details about the creator\'s dates.', 'historical-digital-resources' ); ?></span>
	<p>
		<input type="text" class="widefat" name="<?php $metabox->the_name( 'date_notes' ); ?>" value="<?php $metabox->the_value( 'date_notes' ); ?>"/>
	</p>

<!-- Internal notes -->
	<label><?php _e( 'Internal Notes', 'historical-digital-resources' ); ?></label>
	<p>
		<?php $metabox->the_field('creator_private_notes'); ?>
		<span><?php _e( 'These will never be seen by the public.', 'historical-digital-resources' ); ?></span>
		<textarea name="<?php $metabox->the_name(); ?>" rows="3" class="widefat"><?php $metabox->the_value(); ?></textarea>
	</p>

</div>