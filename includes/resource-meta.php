<div class="hdr-metabox">

<!-- URL -->
	<label><?php _e( 'URL', 'historical-digital-resources'); ?></label>
	<span class="info"><?php _e( 'URL of the resource website', 'historical-digital-resources' ); ?></span>
	<p>
		<input type="url" class="widefat" name="<?php $metabox->the_name( 'url' ); ?>" value="<?php $metabox->the_value( 'url' ); ?>" placeholder="http://"/>
	</p>

<!-- Contributors -->
	<label><?php _e( 'Contributors', 'historical-digital-resources' ); ?></label>
	<span class="info"><?php _e( 'Name(s) of primary people involved in making this resource', 'historical-digital-resources' ); ?></span>
 
 	<div class="repeating-field">
		<?php while( $mb->have_fields_and_multi( 'contributors' ) ) { ?>
		<?php $mb->the_group_open(); ?>
	 
			<?php $mb->the_field('name'); ?>
			<p class="add-remove">
				<input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" class="medium" />
				<a href="#" class="dodelete icon-minus-circled" title="<?php _e( 'Remove This Contributor', 'historical-digital-resources' ); ?>"></a>
			</p>
	 
		<?php $mb->the_group_close(); ?>
		<?php } ?>
		<a href="#" class="docopy-contributors add-another icon-plus-circled" title="<?php _e( 'Add Another Contributor', 'historical-digital-resources' ); ?>"></a>
	</div>

<!-- Institution -->
	<label><?php _e( 'Institution/Publisher', 'historical-digital-resources'); ?></label>
	<span class="info"><?php _e( 'Name(s) of publisher(s) or organization(s) responsible for the creation of this resource, if known.', 'historical-digital-resources' ); ?></span>
	<div class="repeating-field">
		<?php while( $mb->have_fields_and_multi( 'inst_pub' ) ) { ?>
		<?php $mb->the_group_open(); ?>
	 
			<?php $mb->the_field('name'); ?>
			<p class="add-remove">
				<input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" class="medium" />
				<a href="#" class="dodelete icon-minus-circled" title="<?php _e( 'Remove This Institution', 'historical-digital-resources' ); ?>"></a>
			</p>
	 
		<?php $mb->the_group_close(); ?>
		<?php } ?>
		<a href="#" class="docopy-inst_pub add-another icon-plus-circled" title="<?php _e( 'Add Another Institution', 'historical-digital-resources' ); ?>"></a>
	</div>


<!-- Dates -->
	<label><?php _e( 'Medieval Dates', 'historical-digital-resources' ); ?></label>
	<span class="info"><?php _e( 'Enter the start and end dates of when the primary source materials on the website were created and used.', 'historical-digital-resources' ); ?></span>
	<p>
		<?php _e( 'From', 'historical-digital-resources' ); ?><input type="text" class="date" name="<?php $metabox->the_name( 'start_date' ); ?>" value="<?php $metabox->the_value( 'start_date' ); ?>"/>
		&nbsp;<?php _e( 'To', 'historical-digital-resources' ); ?>&nbsp;<input type="text" class="date" name="<?php $metabox->the_name( 'end_date' ); ?>" value="<?php $metabox->the_value( 'end_date' ); ?>"/>
	</p>

<!-- Date notes -->
	<label><?php _e( 'Date Notes', 'historical-digital-resources' ); ?></label>
	<span><?php _e( 'Further information about the coverage dates', 'historical-digital-resources' ); ?></span>
	<p>
		<?php $metabox->the_field('date_notes'); ?>
		<textarea name="<?php $metabox->the_name(); ?>" rows="3" class="widefat"><?php $metabox->the_value(); ?></textarea>
	</p>

<!-- Language notes -->
	<label><?php _e( 'Language Notes', 'historical-digital-resources' ); ?></label>
	<span><?php _e( 'Enter other information on language of primary sources included in resource: e.g. particular dialects, if there is a facing-column translation, if a translation is based on a particular edition of the text, etc.', 'historical-digital-resources' ); ?></span>
	<p>
		<?php $metabox->the_field('language_notes'); ?>
		<textarea name="<?php $metabox->the_name(); ?>" rows="3" class="widefat"><?php $metabox->the_value(); ?></textarea>
	</p>

<!-- Region notes -->
	<label><?php _e( 'Geo-Political Region Notes', 'historical-digital-resources' ); ?></label>
	<span><?php _e( 'Enter other information on specific villages, towns, counties or regions that receive major coverage in the website. Do not note places of libraries or archives where manuscripts or art objects are now located; if relevant, this information may be noted in the Description.', 'historical-digital-resources' ); ?></span>
	<p>
		<?php $metabox->the_field('region_notes'); ?>
		<textarea name="<?php $metabox->the_name(); ?>" rows="3" class="widefat"><?php $metabox->the_value(); ?></textarea>
	</p>

<!-- Genre notes -->
	<label><?php _e( 'Type/Genre Notes', 'historical-digital-resources' ); ?></label>
	<span><?php _e( 'Comments or clarification about the original work type/genre.', 'historical-digital-resources' ); ?></span>
	<p>
		<?php $metabox->the_field('genre_notes'); ?>
		<textarea name="<?php $metabox->the_name(); ?>" rows="3" class="widefat"><?php $metabox->the_value(); ?></textarea>
	</p>

<!-- Subject notes -->
	<label><?php _e( 'Subject Notes', 'historical-digital-resources' ); ?></label>
	<span><?php _e( 'Comments or clarification about the subject headings.', 'historical-digital-resources' ); ?></span>
	<p>
		<?php $metabox->the_field('subject_details'); ?>
		<textarea name="<?php $metabox->the_name(); ?>" rows="3" class="widefat"><?php $metabox->the_value(); ?></textarea>
	</p>

<!-- Creator notes -->
	<label><?php _e( 'Medieval Creator Notes', 'historical-digital-resources' ); ?></label>
	<span><?php _e( 'Comments or clarification about the medieval creator(s), including artists, authors, composers, etc.', 'historical-digital-resources' ); ?></span>
	<p>
		<?php $metabox->the_field('creator_notes'); ?>
		<textarea name="<?php $metabox->the_name(); ?>" rows="3" class="widefat"><?php $metabox->the_value(); ?></textarea>
	</p>

<!-- Additional Catalogers -->
	<label><?php _e( 'Additional Catalogers', 'historical-digital-resources' ); ?></label>
	<span class="info"><?php _e( 'Name(s) of people who contributed to this record.', 'historical-digital-resources' ); ?></span>
 
	 <div class="repeating-field">
		<?php while($mb->have_fields_and_multi('catalogers')): ?>
		<?php $mb->the_group_open(); ?>
	 
			<?php $mb->the_field('cataloger_name'); ?>
			<p class="add-remove">
				<select name="<?php $mb->the_name(); ?>">
					<option value=""></option>
					<?php $catalogers = get_users( 'orderby=nicename' );
					foreach ( $catalogers as $cataloger ) { ?>
						<option value="<?php echo $cataloger->ID; ?>"<?php if( $mb->get_the_value() == $cataloger->ID ) echo $selected; ?>><?php echo $cataloger->display_name; ?></option>
					<?php } ?>
				</select>
				<a href="#" class="dodelete icon-minus-circled" title="<?php _e( 'Remove This Cataloger', 'historical-digital-resources' ); ?>"></a>
			</p>
	 
		<?php $mb->the_group_close(); ?>
		<?php endwhile; ?>
		<a href="#" class="docopy-catalogers add-another icon-plus-circled" title="<?php _e( 'Add Another Cataloger', 'historical-digital-resources' ); ?>"></a>
	</div>


<!-- Live -->
	<label><?php _e( 'Live', 'historical-digital-resources' ); ?></label>
	<?php $metabox->the_field('live'); ?>
	<input type="checkbox" name="<?php $metabox->the_name(); ?>" value="1"<?php if ($metabox->get_the_value()) echo ' checked="checked"'; ?>/> <?php _e( 'Check this box to make this resource visible to the public', 'historical-digital-resources' ); ?>

<!-- Review -->
	<fieldset>
		<legend><?php _e( 'Reviews', 'historical-digital-resources'); ?></legend> 
 
		<?php while($mb->have_fields_and_multi('review')): ?>
		<?php $mb->the_group_open(); ?>
			
			<div class="third-container">
				<div class="third one">
					<?php $mb->the_field('reviewer'); ?>
					<label><?php _e( 'Reviewer', 'historical-digital-resources' ); ?></label>
					<p>
						<select name="<?php $mb->the_name(); ?>">
						<option value=""></option>
						<?php $catalogers = get_users( 'orderby=nicename' );
						foreach ( $catalogers as $cataloger ) { ?>
							<option value="<?php echo $cataloger->ID; ?>"<?php if( $mb->get_the_value() == $cataloger->ID ) echo $selected; ?>><?php echo $cataloger->user_nicename; ?></option>
						<?php } ?>
					</p>
				</select>
				</div>
		 
				<div class="third two">
					<?php $mb->the_field('date'); ?>
					<label><?php _e( 'Date of Review', 'historical-digital-resources' ); ?></label>
					<p><input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/></p>
			 	</div>

			 	<div class="third three">
			 		<a href="#" class="dodelete button"><?php _e( 'Delete This Review', 'historical-digital-resources' ); ?></a>
			 	</div>
			 </div>

			<?php $mb->the_field('review_text'); ?>
			<textarea name="<?php $mb->the_name(); ?>" class="widefat" rows="3"><?php $mb->the_value(); ?></textarea>
			
		<?php $mb->the_group_close(); ?>
		<?php endwhile; ?>
	 
		<p style="margin-bottom:15px; padding-top:5px;"><a href="#" class="docopy-review button"><?php _e( 'Add Another Review', 'historical-digital-resources' ); ?></a></p>

	</fieldset>

<!-- Internal notes -->
	<label><?php _e( 'Internal Notes', 'historical-digital-resources' ); ?></label>
	<p>
		<?php $metabox->the_field('subject_notes'); ?>
		<span><?php _e( 'These will never be seen by the public.', 'historical-digital-resources' ); ?></span>
		<textarea name="<?php $metabox->the_name(); ?>" rows="3" class="widefat"><?php $metabox->the_value(); ?></textarea>
	</p>

</div>