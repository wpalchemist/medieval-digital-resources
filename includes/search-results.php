<?php
// Search Results Page

$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

$args = array( 
	'post_type' => 'resource',
    'order' => 'ASC',
    'orderby' => 'title',
	'paged' => $paged,
	'meta_query' => array(
		array(
			'key' => '_hdr_live',
			'value' => '1',
			'type' => 'CHAR',
			'compare' => '=',
       ),
	),
);

if( isset( $_POST['hdr-subject'] ) || isset( $_POST['hdr-genre'] ) || isset( $_POST['hdr-region'] ) || isset( $_POST['original_language'] ) || isset( $_POST['resource_type'] ) || isset( $_POST['license'] ) || isset( $_POST['modern_language'] ) ) {
	$args['tax_query'] = array(
		'relation' => 'AND',
	);
	if( isset( $_POST['hdr-subject'] ) ) {
		$args['tax_query'][] = array(
		        'taxonomy' => 'subject',
		        'field' => 'slug',
		        'terms' => $_POST['hdr-subject'],
		        'include_children' => true,
		        'operator' => 'IN'
		    );
	}
	if( isset( $_POST['hdr-genre'] ) ) {
		$args['tax_query'][] = array(
		        'taxonomy' => 'genre',
		        'field' => 'slug',
		        'terms' => $_POST['hdr-genre'],
		        'include_children' => true,
		        'operator' => 'IN'
		    );
	}
	if( isset( $_POST['hdr-region'] ) ) {
		$args['tax_query'][] = array(
		        'taxonomy' => 'region',
		        'field' => 'slug',
		        'terms' => $_POST['hdr-region'],
		        'include_children' => true,
		        'operator' => 'IN'
		    );
	}
	if( isset( $_POST['hdr-original_language'] ) ) {
		$args['tax_query'][] = array(
		        'taxonomy' => 'original_language',
		        'field' => 'slug',
		        'terms' => $_POST['hdr-original_language'],
		        'include_children' => true,
		        'operator' => 'IN'
		    );
	}
	if( isset( $_POST['hdr-resource_type'] ) ) {
		$args['tax_query'][] = array(
		        'taxonomy' => 'resource_type',
		        'field' => 'slug',
		        'terms' => $_POST['hdr-resource_type'],
		        'include_children' => true,
		        'operator' => 'IN'
		    );
	}
	if( isset( $_POST['hdr-license'] ) ) {
		$args['tax_query'][] = array(
		        'taxonomy' => 'license',
		        'field' => 'slug',
		        'terms' => $_POST['hdr-license'],
		        'include_children' => true,
		        'operator' => 'IN'
		    );
	}
	if( isset( $_POST['hdr-language'] ) ) {
		$args['tax_query'][] = array(
		        'taxonomy' => 'language',
		        'field' => 'slug',
		        'terms' => $_POST['hdr-language'],
		        'include_children' => true,
		        'operator' => 'IN'
		    );
	}
}
  
if( !empty( $_POST['hdr-begin-date'] ) ) {
	$args['meta_query'][] = array(
		'key' => '_hdr_start_date',
		'value' => $_POST['hdr-begin-date'],
		'type' => 'NUMERIC',
		'compare' => '<=',
   );
}
if( !empty( $_POST['hdr-end-date'] ) ) {
	$args['meta_query'][] = array(
		'key' => '_hdr_end_date',
		'value' => $_POST['hdr-end-date'],
		'type' => 'NUMERIC',
		'compare' => '<=',
   );
}

if( isset( $_POST['hdr-creator'] ) ) {
	$args['connected_type'] = 'resources_to_creators';
	$args['connected_items'] = $_POST['hdr-creator'];
}
  
if( '' !== $_POST['hdr-search-text'] ) {
	$args2 = array(
			'fields' => 'ids',
			'post_type' => 'resource',
			'posts_per_page' => -1,
			'meta_query' => array(
					array(
							'key' => '_hdr_live',
							'value' => '1',
							'type' => 'CHAR',
							'compare' => '=',
					),
			),
			's' => $_POST['hdr-search-text'],
	);

	$args3 = array(
			'fields' => 'ids',
			'post_type' => 'resource',
			'posts_per_page' => -1,
			'meta_query' => array(
					'relation' => 'AND',
					array(
							'key' => '_hdr_live',
							'value' => '1',
							'type' => 'CHAR',
							'compare' => '=',
					),
					array(
						'relation' => 'OR',
						array(
								'key' => '_hdr_date_notes',
								'value' => $_POST['hdr-search-text'],
								'type' => 'CHAR',
								'compare' => 'LIKE',
						),
						array(
								'key' => '_hdr_language_notes',
								'value' => $_POST['hdr-search-text'],
								'type' => 'CHAR',
								'compare' => 'LIKE',
						),
						array(
								'key' => '_hdr_region_notes',
								'value' => $_POST['hdr-search-text'],
								'type' => 'CHAR',
								'compare' => 'LIKE',
						),
						array(
								'key' => '_hdr_genre_notes',
								'value' => $_POST['hdr-search-text'],
								'type' => 'CHAR',
								'compare' => 'LIKE',
						),
						array(
								'key' => '_hdr_subject_details',
								'value' => $_POST['hdr-search-text'],
								'type' => 'CHAR',
								'compare' => 'LIKE',
						),
						array(
								'key' => '_hdr_creator_notes',
								'value' => $_POST['hdr-search-text'],
								'type' => 'CHAR',
								'compare' => 'LIKE',
						),
					),
			),
	);
}

if( isset( $args2 ) ) {
	// construct query from three sets of args
	$args['fields'] = 'ids';
	$search1 = get_posts( $args );
	if( empty( $search1 ) ) { ?>
		<p><?php _e( 'Sorry, your search returned no results', 'historical-digital-resources' ); ?></p>
		<?php return;
	}
	$search2 = get_posts( $args2 );
	$search3 = get_posts( $args3 );

	if( !empty( $search2 ) && !empty( $search3 ) ) {
		$post_ids = array_intersect( $search1, $search2, $search3 );
	} elseif( !empty( $search2 ) && empty( $search3 ) ) {
		$post_ids = array_intersect( $search1, $search2 );
	} elseif( empty( $search2 ) && !empty( $search3 ) ) {
		$post_ids = array_intersect( $search1, $search3 );
	}

	if( empty( $post_ids ) ) { ?>
		<p><?php _e( 'Sorry, your search returned no results', 'historical-digital-resources' ); ?></p>
		<?php return;
	}

	$merged_args = array(
			'post_type' => 'resource',
			'order' => 'ASC',
			'orderby' => 'title',
			'paged' => $paged,
			'post__in' => $post_ids,
	);

	$search_query = new WP_Query( $merged_args );

} else {
	$search_query = new WP_Query( $args );
}

if ( $search_query->have_posts() ) {
	while ( $search_query->have_posts() ) : $search_query->the_post(); ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header>
				<h2>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				</h2>
			</header>
			<div>
				<?php the_post_thumbnail( 'thumbnail', array( 'class'=>'alignleft' ) ); ?>
				<?php the_excerpt(); ?>
			</div>
		</article>
	<?php endwhile;

	$total_pages = $search_query->max_num_pages;
	if ($total_pages > 1){
		$current_page = max(1, get_query_var('paged'));
		$big = 999999999; ?>
		<div class='pagination'>
	  	<?php echo paginate_links( array(
			'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format' => '?paged=%#%',
			'current' => max( 1, get_query_var('paged') ),
			'total' => $search_query->max_num_pages
		) ); ?>
	    </div>
	<?php }
} else { ?>
	<p><?php _e( 'Sorry, your search returned no results', 'historical-digital-resources' ); ?></p>
<?php }

// Reset Post Data
wp_reset_postdata();

?>