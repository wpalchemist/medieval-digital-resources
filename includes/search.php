<?php
// Search Page
?>

<form method="post" action="<?php the_permalink(); ?>" id="hdr-search">
	<input type="submit" class="secondary-submit" value="<?php _e( 'Search', 'historical-digital-resources' ); ?>">

	<p>
		<label><?php _e( 'Search Term', 'historical-digital-resources' ); ?></label>
		<input type="text" id="hdr-search-text" name="hdr-search-text">
	</p>
	<p>
		<label><?php _e( 'Dates of Source Material', 'historical-digital-resources' ); ?></label>
		<?php _e( 'From', 'historical-digital-resources' ); ?>:&nbsp;<input type="number" min="0" max="2015" class="hdr-date" id="hdr-begin-date" name="hdr-begin-date">&nbsp;
		<?php _e( 'To', 'historical-digital-resources' ); ?>:&nbsp;<input type="number" min="0" max="2015" class="hdr-date" id="hdr-end-date" name="hdr-end-date">
	</p>
	<p class="hdr-select-field">
		<label><?php _e( 'Subject of Source Material', 'historical-digital-resources' ); ?></label>
		<?php echo hdr_display_taxonomy_field_picker( 'subject', __( 'Subject Heading', 'historical-digital-resources' ) ); ?>
	</p>
	<p class="hdr-select-field">
		<label><?php _e( 'Genre/Document Type of Source Material', 'historical-digital-resources' ); ?></label>
		<?php echo hdr_display_taxonomy_field_picker( 'genre', __( 'Genre/Document Type', 'historical-digital-resources' ) ); ?>
	</p>
	<p class="hdr-select-field">
		<label><?php _e( 'Geopolitical Region', 'historical-digital-resources' ); ?></label>
		<?php echo hdr_display_taxonomy_field_picker( 'region', __( 'Geopolitical Region', 'historical-digital-resources' ) ); ?>
	</p>
	<p class="hdr-select-field">
		<label><?php _e( 'Original Language', 'historical-digital-resources' ); ?></label>
		<?php echo hdr_display_taxonomy_field_picker( 'original_language', __( 'Original Language', 'historical-digital-resources' ) ); ?>
	</p>
	<p class="hdr-select-field">
		<label><?php _e( 'Author/Creator of Source Material', 'historical-digital-resources' ); ?></label>
		<select class="hdr-select" data-placeholder="Select Creator" name="hdr-creator[]" multiple>
			<?php 
			$creators = get_posts( array(
				'supress_filters' => false,
				'post_type' => 'creator',
				'posts_per_page' => -1,
				'connected_type' => 'resources_to_creators',
				'connected_direction' => 'from',
				'connected_items' => 'any',
				'orderby' => 'title' ,
				'order' => 'ASC',
			) );
			//remove duplicates
			foreach ( $creators as $post ) {
			  $creators[ $post->ID ] = $post;
			} 
			foreach( $creators as $creator ) { ?>
				<option value="<?php echo $creator->ID; ?>"><?php echo $creator->post_title; ?></option>
			<?php } ?>
		</select>
	</p>
	<p class="hdr-select-field">
		<label><?php _e( 'Type of Resource', 'historical-digital-resources' ); ?></label>
		<?php echo hdr_display_taxonomy_field_picker( 'resource_type', __( 'Resource Type', 'historical-digital-resources' ) ); ?>
	</p>
	<p class="hdr-select-field">
		<label><?php _e( 'License', 'historical-digital-resources' ); ?></label>
		<?php echo hdr_display_taxonomy_field_picker( 'license', __( 'License', 'historical-digital-resources' ) ); ?>
	</p>
	<p class="hdr-select-field">
		<label><?php _e( 'Language of Resource', 'historical-digital-resources' ); ?></label>
		<?php echo hdr_display_taxonomy_field_picker( 'modern_language', __( 'Resource Language', 'historical-digital-resources' ) ); ?>
	</p>
	<p class="hdr-select-field">
		<label><?php _e( 'Language of Modern Translation', 'historical-digital-resources' ); ?></label>
		<?php echo hdr_display_taxonomy_field_picker( 'translation_language', __( 'Translation Language', 'historical-digital-resources' ) ); ?>
	</p>

	<input type="submit" value="<?php _e( 'Search', 'historical-digital-resources' ); ?>">
</form>

<?php

function hdr_display_taxonomy_field_picker( $taxonomy, $label ) {
	$terms = get_terms( $taxonomy );
	if( is_array( $terms ) && !empty( $terms ) ) {
		$termlist = array();
		$select = '<select class="hdr-select" data-placeholder="Select ' . $label . '" name="hdr-' . $taxonomy . '[]" multiple>';
		foreach( $terms as $term ) {
			if( '0' == $term->parent ) {
            	// $select .= '<option value="' . $term->slug . '">' . $term->name . '</option>';
            	$termlist[$term->slug] = $term->name;
            } else {
            	$parent = get_term( $term->parent, $taxonomy );
            	// $select .= '<option value="' . $term->slug . '">' . $parent->name . ' - ' . $term->name . '</option>';
            	$termlist[$term->slug] = $parent->name . ' - ' . $term->name;
            }
        }
        asort( $termlist );
        foreach( $termlist as $value => $name ) {
        	$select .= '<option value="' . $value . '">' . $name . '</option>';
        }
        $select .= '</select>';
	} else {
		$select = sprintf( __( 'No %ss are available', 'historical-digital-resources' ), $label );
	}

    return $select;
}