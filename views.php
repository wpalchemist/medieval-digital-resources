<?php
// all of the views go in this file

// ------------------------------------------------------------------------
// SINGLE RESOURCE VIEW
// ------------------------------------------------------------------------

// Enqueue stylesheet for single resources and taxonomies
function hdr_enqueue_resource_style() {
	$taxonomies = array(
		'subject',
		'genre',
		'region',
		'modern_language',
		'translation_language',
		'original_language',
		'license',
		'resource_type',
		);
	if( is_singular( 'resource' ) || is_singular( 'creator' ) || is_tax( $taxonomies ) ) {
		wp_enqueue_style( 'hdr-style', plugin_dir_url(__FILE__) . 'css/historical-digital-resources.css' );
	}
}

add_action( 'wp_enqueue_scripts', 'hdr_enqueue_resource_style' );

// Display single resource (filter content)
function hdr_single_resource_view( $content ) {
	if( is_singular( 'resource' ) ) {
		$post_id = get_the_id();
		global $resource_metabox;
		$meta = $resource_metabox->the_meta();

		if( !isset( $meta['live'] ) || "1" !== $meta['live'] ) {
			// this one isn't live, so let's bail fast!
			$single_view = __( 'This resource is not available to the public', 'historical-digital-resources' );
			return $single_view;
		}

		$single_view = '
		<div id="hdr-single-resource">
			<div class="hdr-description clearfix">';
				// Featured Image
				$single_view .= '<a href="' . $meta['url'] . '" title="' . get_the_title() . '">';
				if( has_post_thumbnail() ) {
					$single_view .= get_the_post_thumbnail( $post_id, 'thumbnail', array( 'class' => 'alignleft' ) );
				}
				$single_view .= '</a>';
				$single_view .= '<p class="hdr-link">' . __( 'Link:', 'historical-digital-resources' ) . '&nbsp;<a href="' . $meta['url'] . '" title="' . get_the_title() . '">' . $meta['url'] . '</a></p>';
				// Description
				$single_view .= $content;
				$single_view .= '
			</div>
			<div class="hdr-details">
				<div class="hdr-resource-details">
					<h4>' . __( 'Resource details:', 'historical-digital-resources' ) . '</h4>';
					// Resource Type
					$single_view .= hdr_display_term_list( $post_id, 'resource_type', __( 'Resource Type', 'historical-digital-resources' ) );
					$single_view .= hdr_display_term_list( $post_id, 'license', __( 'License', 'historical-digital-resources' ) );
					$single_view .= hdr_display_term_list( $post_id, 'modern_language', __( 'Modern Language', 'historical-digital-resources' ) );
					$single_view .= hdr_display_term_list( $post_id, 'translation_language', __( 'Language of Modern Translation', 'historical-digital-resources' ) );

				$single_view .= '
				</div>
				<div class="hdr-historical-details">
					<h4>' . __( 'Medieval primary source details:', 'historical-digital-resources' ) . '</h4>';
					$single_view .= '<p><span class="hdr-field-label">' . __( 'Dates:', 'historical-digital-resources' ) . '</span>&nbsp;' . $meta['start_date'] . '&nbsp;-&nbsp;' . $meta['end_date'] . '</p>';
					if( isset( $meta['date_notes'] ) && '' !== $meta['date_notes'] ) {
						$single_view .= '<p class="hdr-details">' . $meta['date_notes'] . '</p>';
					}
					$single_view .= hdr_display_term_list( $post_id, 'subject', __( 'Subject', 'historical-digital-resources' ) );
					if( isset( $meta['subject_details'] ) && '' !== $meta['subject_details'] ) {
						$single_view .= '<p class="hdr-details">' . $meta['subject_details'] . '</p>';
					}
					$single_view .= hdr_display_term_list( $post_id, 'genre', __( 'Type/Genre of Medieval Primary Source Material', 'historical-digital-resources' ) );
					if( isset( $meta['genre_notes'] ) && '' !== $meta['genre_notes'] ) {
						$single_view .= '<p class="hdr-details">' . $meta['genre_notes'] . '</p>';
					}
					$single_view .= hdr_display_term_list( $post_id, 'region', __( 'Geopolitical Region', 'historical-digital-resources' ) );
					if( isset( $meta['region_notes'] ) && '' !== $meta['region_notes'] ) {
						$single_view .= '<p class="hdr-details">' . $meta['region_notes'] . '</p>';
					}
					$single_view .= hdr_display_term_list( $post_id, 'original_language', __( 'Original Language', 'historical-digital-resources' ) );
					if( isset( $meta['language_notes'] ) && '' !== $meta['language_notes'] ) {
						$single_view .= '<p class="hdr-details">' . $meta['language_notes'] . '</p>';
					}
					// Find connected creator(s)
					$creators = new WP_Query( array(
						'connected_type' => 'resources_to_creators',
						'connected_items' => $post_id,
						'nopaging' => true,
					) );

					if ( $creators->have_posts() ) {
						$single_view .= '<p class="hdr-creators"><span class="hdr-field-label">' . __( 'Medieval Creators', 'historical-digital-resources' ) . ':&nbsp;</span>';
						$i = 1;
						while ( $creators->have_posts() ) : $creators->the_post();
							if( $i !== 1 ) {
								$single_view .= ",&nbsp;";
							}
							$single_view .= '<a href="' . get_the_permalink() . '">' . get_the_title() . '</a>';
							$i++;
						endwhile;
						wp_reset_postdata();
					}
					if( isset( $meta['creator_notes'] ) && '' !== $meta['creator_notes'] ) {
						$single_view .= '<p class="hdr-details">' . $meta['creator_notes'] . '</p>';
					}
				$single_view .= '
				</div>
			</div>
		</div>';

		$single_view = apply_filters( 'hdr_single_view', $single_view );
		return $single_view;
	} else {
		return $content;
	}
}
add_filter( 'the_content', 'hdr_single_resource_view' );

function hdr_display_term_list( $post_id, $taxonomy, $taxonomy_name ) {
	$term_list = '';
	$terms = get_the_terms( $post_id, $taxonomy );
	if( $terms ) {
		$term_list .= '<p class="hdr-' . $taxonomy . '"><span class="hdr-field-label">' . $taxonomy_name . ':&nbsp;</span>';
		$i = 1;
		foreach( $terms as $term ) {
			if( $i !== 1 ) {
				$term_list .= ",&nbsp;";
			}
			$term_list .= '<a href="' . get_term_link( $term ) . '">' . $term->name . '</a>';
			$i++;
		}
		$term_list .= '</p>';
	}

	return $term_list;
}



// ------------------------------------------------------------------------
// SINGLE CREATOR VIEW
// ------------------------------------------------------------------------


// Display single creator (filter content)
function hdr_single_creator_view( $content ) {
	if( is_singular( 'creator' ) ) {
		$post_id = get_the_id();
		global $creator_meta;
		$meta = $creator_meta->the_meta();

		$single_view = '
		<div id="hdr-single-creator">';
			// Featured Image
			if( has_post_thumbnail() ) {
				$single_view .= get_the_post_thumbnail( $post_id, 'thumbnail', array( 'class' => 'alignleft' ) );
			}
			// Description
			if( isset( $meta['title'] ) ) {
				$single_view .= '<p class="hdr-creator-title"><span class="hdr-field-label">' . __( 'Title: ', 'historical-digital-resources' ) . '</span>' . $meta['title'] . '</p>';
			}
			if( isset( $meta['alias'] ) ) {
				$single_view .= '<p class="hdr-creator-alias"><span class="hdr-field-label">' . __( 'Alternate names/spellings: ', 'historical-digital-resources' ) . '</span>' . $meta['alias'] . '</p>';
			}
			if( isset( $meta['start_date'] ) || isset( $meta['end_date'] ) ) {
				$single_view .= '<p class="hdr-creator-dates"><span class="hdr-field-label">' . __( 'Dates: ', 'historical-digital-resources' ) . '</span>';
				if( isset( $meta['start_date'] ) ) {
					$single_view .= $meta['start_date'];
				}
				$single_view .= '&nbsp;-&nbsp;';
				if( isset( $meta['end_date'] ) ) {
					$single_view .= $meta['end_date'];
				}
				if( isset( $meta['date_notes'] ) ) {
					$single_view .= '&nbsp;(' . $meta['date_notes'] . ')&nbsp;';
				}
				$single_view .= '</p>';
			}

			$single_view .= '</p>';
			$single_view .= $content;
			// Find connected resource(s)
			$resources = new WP_Query( array(
				'connected_type' => 'resources_to_creators',
				'connected_items' => get_the_id(),
				'nopaging' => true,
			) );

			if ( $resources->have_posts() ) {
				$single_view .= '<p class="hdr-resources"><span class="hdr-field-label">' . __( 'Resources', 'historical-digital-resources' ) . ':&nbsp;</span>';
				$i = 1;
				while ( $resources->have_posts() ) : $resources->the_post();
					if( $i !== 1 ) {
						$single_view .= ",&nbsp;";
					}
					$single_view .= '<a href="' . get_the_permalink() . '">' . get_the_title() . '</a>';
					$i++;
				endwhile;
				wp_reset_postdata();
			}
			$single_view .= '
		</div>';

		$single_view = apply_filters( 'hdr_single_creator_view', $single_view );
		return $single_view;
	} else {
		return $content;
	}
}
add_filter( 'the_content', 'hdr_single_creator_view' );




// ------------------------------------------------------------------------
// SEARCH SHORTCODE
// ------------------------------------------------------------------------
function hdr_search_shortcode() {

	// enqueue style
	wp_enqueue_style( 'historical-digital-resources-style', plugin_dir_url(__FILE__) . 'css/historical-digital-resources.css' );
	// enqueue chosen
	wp_enqueue_script( 'chosen', plugin_dir_url(__FILE__) . 'js/chosen.jquery.min.js' );

	// Search Results
	if( $_POST ) {
		ob_start();
		echo '<div id="hdr-search-results">';
	    include plugin_dir_path(__FILE__) . 'includes/search-results.php';
	    echo '</div>';
	    echo '<h3>' . __( 'Search Again', 'historical-digital-resources' ) . '</h3>';
	    include plugin_dir_path(__FILE__) . 'includes/search.php';
	    return ob_get_clean();
	}
	
	// Search Form
	ob_start();
    include plugin_dir_path(__FILE__) . 'includes/search.php';
    return ob_get_clean();

}
add_shortcode( 'hdr_search', 'hdr_search_shortcode' );


// ------------------------------------------------------------------------
// TERM LIST SHORTCODE
// ------------------------------------------------------------------------

function hdr_term_list_shortcode( $atts ) {

	// Attributes
	extract( shortcode_atts(
		array(
			'taxonomy' => '',
			'show_description' => 'true',
		), $atts )
	);

	$term_list = '';

	if( '' == $taxonomy ) {
		$term_list .= '<p>' . __( 'You must select a taxonomy.', 'historical-digital-resources' ) . '</p>';
		return $term_list;
	}

	// enqueue style
	wp_enqueue_style( 'historical-digital-resources-style', plugin_dir_url(__FILE__) . 'css/historical-digital-resources.css' );

	$terms = get_terms( $taxonomy, 'parent=0' );

	if( empty( $terms ) ) {
		$term_list .= '<p>' . __( 'Sorry, no terms were found.', 'historical-digital-resources' ) . '</p>';
		return $term_list;
	} 

	$term_list .= '<ul id="hdr-term-list">';
	foreach( $terms as $term ) {
		$term_list .= '<li class="' . $term->slug . '"><a href="' . get_term_link( $term ) .  '">' . $term->name . '</a>';
		if( 'true' == $show_description && isset( $term->description ) && '' !== $term->description ) {
			$term_list .= ': ' . $term->description;
		}
		$children = get_terms( $taxonomy, 'parent='.$term->term_id );
		if( !empty( $children ) ) {
			$term_list .= hdr_get_term_children( $taxonomy, $term->term_id, 1, $show_description );
		}
		$term_list .= '</li>';
	}
	$term_list .= '</ul>';

    return $term_list;

}
add_shortcode( 'hdr_term_list', 'hdr_term_list_shortcode' );

function hdr_get_term_children( $taxonomy, $term_id, $depth, $show_description ) {

	$children = '';
	$childterms = get_terms( $taxonomy, array(
            "parent" => $term_id,
        )
    );

	if( !empty( $childterms ) ) {
		$children .= '<ul>';
		$depth++;
	    foreach( $childterms as $childterm ) {
	    	$children .= '<li class="' . $childterm->slug . '"><a href="' . get_term_link( $childterm ) .  '">' . $childterm->name . '</a>';
			if( 'true' == $show_description && isset( $childterm->description ) && '' !== $childterm->description ) {
				$children .= ': ' . $childterm->description;
			}
	    	$children .= hdr_get_term_children( $taxonomy, $childterm->term_id, $depth, $show_description );
	    }
	    $children .= '</ul>';
	}

	return $children;
}

?>